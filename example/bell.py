import isq
from isq import LocalDevice

ld = LocalDevice()

@isq.qpu(ld)
def bell():
    '''
    qbit a, b;
    H(a);
    CNOT(a, b);
    M(a);
    M(b);
    '''

res = bell()
print(res)

isq_str = '''
    qbit q[2];
    H(q[0]);
    CNOT(q[0], q[1]);
    M(q[0,1]);
'''
res = ld.run(isq_str)
print(res)