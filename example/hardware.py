from isq import QcisDevice, AwsDevice

isq_str = '''
    qbit q[2];
    H(q[0]);
    CNOT(q[0], q[1]);
    M(q[0,1]);
'''

# qcis hardware
qd = QcisDevice(user = "xxx", passwd = "xxx")
qcsi_task = qd.run(isq_str)
print(qcsi_task.state)
res = qcsi_task.result()
print(res)

#aws hardware
ad = ad = AwsDevice(shots=100, device_arn = "arn:aws:braket:::device/qpu/ionq/ionQdevice", s3 = ("amazon-braket-arclight", "simulate"), max_wait_time=10)
aws_task = ad.run(isq_str)
print(aws_task.state)
res = aws_task.result()
print(res)