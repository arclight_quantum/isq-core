
import isq
from isq import LocalDevice, optv

#定义量子线路
isq_str = '''
    qbit q[2];
    RX(theta[0], q[0]);
    RY(theta[1], q[1]);
    M(q[0]);
'''

#定义device
ld = LocalDevice()

#定义计算函数
def calc(params):
    #使用probs函数进行带参编译和模拟，其中需要优化的参数通过optv封装后传入
    c = ld.probs(isq_str, theta = optv(params))
    return c[0] - c[1]

#定义grad，并调用，对第0个参数求微分
g = isq.grad(calc, [0])
d = g([0.2, 0.4])
print(d)